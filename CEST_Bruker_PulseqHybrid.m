function [floc,fname] = CEST_Bruker_PulseqHybrid()
%% Pulseq-CEST interpeter for Bruker systems
% 
%%%%%%%%%%%%%%%%% SM @ MPI Tueb %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% code is now completely re-worked: the user may select an existing Bruker
% readout (entire method is requiered!) and a pulse *.seq file containing a
% presaturation block for CEST; both will be combined automatically into a
% new Bruker method which can then be installed at the scanner (ParaVision
% 6);
% bug fixes:    sebastian.mueller@tubeingen.mpg.de
% source code:  https://gitlab.com/SebMue/pulseq_cest_hybrid_bruker
% last edit:    November 20, 2020
% %%%%%%%%%%%%%%%%%%%%%%%%%%disclaimer%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright 2020 SM@MPI Tueb
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the "Software"),
% to deal in the Software without restriction, including without limitation
% the rights to use, copy, modify, merge, publish, distribute, sublicense,
% and/or sell copies of the Software, and to permit persons to whom the
% Software is furnished to do so, subject to the following conditions:
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
% THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
% FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
% DEALINGS IN THE SOFTWARE.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Read the Pulseq file
    % select Bruker method source code 
    [fname,floc] = locatePPGfile();
    % get pulseq ready and select *.seq file:
    seq = mr.Sequence();
    [fnPLSQ,pnPLSQ] = uigetfile('*.*','please select *.seq file with CEST presaturation','MultiSelect','off');
    
    try
        if ~isdir([floc,'\CEST'])
            mkdir([floc,'\CEST']);
        end
        % log file to store some information
        diary([floc,'\CEST\MATLAB_logFile.txt']);
        seq.read([pnPLSQ,fnPLSQ]);
        fprintf('files were selected!\n');
%% reading & analyzing *.seq file (might take some time depending on size ...)
        [eventTable_full,eventTable_ID,eventTable,~,unique_rf] = interpretPulseqFile(seq);
%% writing information into arrays --> these will be written to a *.cest file later on
        [CEST_offArray,CEST_delays,CESTphasearray,CEST_rf,CEST_rfFA,CEST_pulseID] = CreateArraysForCEST(fname,floc,eventTable_ID,eventTable_full,unique_rf);
%% transfer pulses from *.seq files into Bruker's *.exc format
        [~,CEST_pwrEquiv,~,CEST_pwrIntRat] = writePulseShapeToFile(seq,unique_rf,floc);
%% we'll copy the *.seq file so information is available for post procesing
        StatCopy = copyfile([pnPLSQ,fnPLSQ] ,[floc,'\CEST\orig_',fnPLSQ]);
        if ~StatCopy
            warning('COULD NOT COPY THE ORIGINAL *.seq FILE!!!');
        end
%% modify Bruker Source Code
        ResID =[0 0 0 0 0 0 0 0];
        methodDirectory     = floc;
        numCESTpulses       = numel(unique(eventTable(eventTable_ID>0)));
        [ResID(1)]          = introduceRFpulse(numCESTpulses,methodDirectory);
        [ResID(2)]          = initRFpulsesInMethod(numCESTpulses,methodDirectory);
        [ResID(3)]          = modifyBackBone(numCESTpulses,methodDirectory);
        [ResID(6)]          = modifyBackBone_timing(methodDirectory); %this has to be called after first modification of backbone.c was performed!!!
        [ResID(4)]          = modifyparsRelations(numCESTpulses,methodDirectory,CEST_rf,CEST_rfFA,CESTphasearray,CEST_delays,CEST_offArray,CEST_pwrEquiv,CEST_pulseID,CEST_pwrIntRat);
        [ResID(5),BaseParamIndex] = modifyBaseLevelRelations(numCESTpulses,methodDirectory,size(eventTable_full,2));
        [ResID(7)]          = modifyXML(numCESTpulses,methodDirectory);
        [ResID(8)]          = modifyParsLayout(methodDirectory);
        
        if sum(find(ResID==0))
            error(sprintf('trouble while modifying file number: %.0f!\n',find(ResID==0)));
        else
            clear ResID numCESTpulses methodDirectory CEST_rf CEST_rfFA CESTphasearray 
            clear CEST_delays CEST_offArray CEST_pwrEquiv CEST_pulseID 
        end
%% write Bruker *.ppg file containing the CEST presaturation 
        if ~WritingCESTpresatToPPG(floc,eventTable_ID,eventTable,eventTable_full,BaseParamIndex,seq,unique_rf)
            error('failed to write CEST presaturation to *.ppg file!');
        else
            clear eventTable eventTable_ID BaseParamIndex seq 
        end
        diary off;
%% on error:        
    catch
        warning('something went wrong...');
        diary off;
    end
end %main function


