function [CEST_offArray,CEST_delays,CESTphasearray,CEST_rf,CEST_rfFA,CEST_pulseID] = CreateArraysForCEST(fname,floc,eventTable_ID,eventTable_full,unique_rf)
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m -----------    
%% we need arrays for offet frequency, phase, pulse shape and pulse duration
    fid = fopen(fname,'r'); %this is the readout
    nn = 1;
    define_lines=[];
    while ~feof(fid)
        tline{nn}=fgetl(fid);
        if nn>1 && contains(tline{nn-1},'#include <MRI.include>') %as last element in define_lines is accessed this should work....
            define_lines = [define_lines,nn]; 
        end
        nn = nn+1;
    end
    fclose(fid);clear fid;

    CEST_offArray   = [];
    CEST_delays     = [];
    CESTphasearray  = [];
    CEST_rf         = []; 
    CEST_rfFA       = [];
    CEST_pulseID    = [];
    indx            = [];
    for nOffset=1:size(eventTable_full,2)
        for nPulse=1:size(eventTable_full,1)
            if eventTable_ID(nPulse,nOffset) > 0  %RF event 
                CEST_offArray   = [CEST_offArray,eventTable_full{nPulse,nOffset}.offset];
                CESTphasearray  = [CESTphasearray,eventTable_full{nPulse,nOffset}.phase];
                CEST_rf         = [CEST_rf,eventTable_full{nPulse,nOffset}.rfDuration];
                CEST_rfFA       = [CEST_rfFA,eventTable_full{nPulse,nOffset}.rfFAHz];
                % all assignments are done w.r.t. the variable "unique_rf"!
                for nn=1:size(unique_rf,1)
                    if sum(eventTable_full{nPulse,nOffset}.ID-unique_rf(nn,:)) == 0
                        indx = nn;
                    end
                end
                CEST_pulseID    = [CEST_pulseID,indx];
            elseif eventTable_ID(nPulse,nOffset) == 0 % a delay
                CEST_delays     = [CEST_delays,eventTable_full{nPulse,nOffset}.delay];
            end
        end
    end

    % switch to peak B1 in [�T] (will be used on Bruker system to determine power!):
    CEST_rfFA = CEST_rfFA / 42.5764; 
    

    lastLine=0;
    while lastLine < size(tline,2)
        if ~isempty(tline{1,end-lastLine}) && ~isnumeric(tline{1,end-lastLine})
            break;
        elseif isnumeric(tline{1,end-lastLine})
            warning(['could not read line ',num2str(size(tline,2)-lastLine),' check *.ppg carefully!']);
        end
        lastLine = lastLine + 1;
    end

    
    fprintf(['=======\n']);
    fprintf(['modified readout *.pgg file is written into:\n\t',strrep(floc,'\','\\'),'\\CEST\\\n'])
    fprintf(['=======\n']);
    if ~isdir([floc,'\CEST']);mkdir([floc,'\CEST']);end

    strt=strfind(fname,'\');
    fid = fopen([floc,'\CEST\',fname(strt(end)+1:end)],'w');clear strt;

    insertedCESTpulseq = 0; %important - keep 0!!!

    for nLine=1:size(tline,2)-lastLine
        skipThisLine=0;
        if nLine == define_lines(end)
            here = cd;cd(floc);
           if ~isdir('CEST')
               mkdir CEST;
           end; cd(here);
           fprintf(fid,'define list<frequency> fCEST = {$CEST_freqList};\n');
           fprintf(fid,'define list<phase> pCEST = {$CEST_PhaseList}\n');    
           fprintf(fid,'define list<delay> dCEST = {$CEST_DelayList}\n');       % required unit Bruker: [s]           
           fprintf(fid,'define list<pulse> rfCEST = {$CEST_RFduration}\n');     % required unit Bruker: [�s] (microsec)
           fprintf(fid,'define list<power> FArfCEST = {$CEST_pwrLIST}\n');      % this will be [W]
           fprintf(fid,'define loopcounter CEST_cnt\n"CEST_cnt = 1"\n\n');      % loop counter
        end

        % modify *.ppg of readout such that NR-loop includes CEST
        % presaturation:
        if ~isempty(tline{1,nLine}) &&  ~isnumeric(tline{1,nLine}) 
           temp_line = strtrim(tline{1,nLine});
           if ischar(tline{1,nLine}) && contains(tline{1,nLine},'start,','IgnoreCase',true)...
                                        && ~insertedCESTpulseq && strcmp(temp_line(1:6),'start,')                             
                fprintf(fid,'startCEST,\n#include<CEST_arbitrary.ppg>\nstopCEST,\n');
                insertedCESTpulseq = 1;
                warning('no match found for "startNR" --> using label "start, 10u" instead !!!');
                warning(sprintf(['\n\t\tmodified the outermost (repetition NR) loop statement;\n\t\tin file:\n\t\t',strrep(fname,'\','\\'),'\n\t\tcheck line %03.f carefully!'],nLine));
           elseif ischar(tline{1,nLine}) && contains(tline{1,nLine},'lo to start times NR','IgnoreCase',true) && insertedCESTpulseq
                skipThisLine=1;
                fprintf(fid,'lo to startCEST times NR\n');
                warning(sprintf(['\n\t\tmodified the outermost (repetition NR) loop statement;\n\t\tin file:\n\t\t',strrep(fname,'\','\\'),'\n\t\tcheck line %03.f carefully!'],nLine));
           end
        end
        if ~isnumeric(tline{1,nLine})
           % to the best of my knowledge the only numeric value that may appear
           % is -1 with indicates that this line could not be read from source
           % file...
           if ~skipThisLine
                fprintf(fid,[tline{1,nLine} ,'\n']);
           end
        elseif isnumeric(tline{1,nLine})
           warning(['could not read line ',num2str(nLine),' check *.ppg carefully!']);
        end
        % be responsible:
        if ~isempty(tline{1,nLine}) &&  ~isnumeric(tline{1,nLine}) 
           if ischar(tline{1,nLine}) && contains(tline{1,nLine},'All Rights Reserved','IgnoreCase',true)
               fprintf(fid,'\n;-*-*-*-*-*-*-*-*-*-*-*-*-*-\n; !!! WIP ONLY - no warranty for this code !!!\n; This file was modified as a pulseq-CEST hybrid!\n; Bug reports: sebastian.mueller<at>tuebingen.mpg.de\n;-*-*-*-*-*-*-*-*-*-*-*-*-*-\n');
           end
        end
    end
    fclose(fid);
end %main function