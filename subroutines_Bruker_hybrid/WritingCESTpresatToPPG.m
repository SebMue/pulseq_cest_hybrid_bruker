function [statusID] = WritingCESTpresatToPPG(floc,eventTable_ID,eventTable,eventTable_full,BaseParamIndex,seq,unique_rf)
% [statusID] = WritingCESTpresatToPPG(floc,eventTable_ID,eventTable,eventTable_full,BaseParamIndex,seq,unique_rf)
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------
%% everything below here needs to be adapted and modified probably
    try

        fid     = fopen([floc,'\CEST\CEST_arbitrary.ppg'],'w');
        fprintf(fid,'; PPG file\n');

        fprintf(fid,';\n');
        fprintf(fid,';\n\n');

        fprintf('WARNING: so far we only support experiements with similar presaturation for each offset!!!\n')
        ID = strrep(strrep(datestr(now),':','-'),' ','_');
        fprintf(fid,';THIS IS WIP! Do not base any diagnosis on this sequence!\n;Source code: sebastian.mueller<at>tuebingen.mpg.de\n;no warranty can be given for this code!\n');
        fprintf(fid,['; this file only includes CEST pre-saturation; written: ',ID,'\n']);
        fprintf(fid,';the CEST pre-saturation block has to be included in the NR-loop, that is each offset has another NR!!!!\n\n\n');
        % reset array pointers
        fprintf(fid,'if "CEST_cnt == 1" goto labF\nlabF2,\nif "CEST_cnt == 1" goto labP\nlabP2,\nif "CEST_cnt == 1" goto labRF\nlabRF2,\nif "CEST_cnt == 1" goto labD\nlabD2,\nif "CEST_cnt == 1" goto labFA\nlabFA2,\n\n');

    % adiaSLexp and others need different pulses for up-/downfield saturation!
    % We'll allow to different cases to cover this. Limiting factor are RF
    % pulses atm, as their shape is hard coded in Bruker's *.ppg files. Also
    % number of pulses must stay similar for alle offsets atm. We might solve
    % that issue by having a list of loop counters , if possible. Still, for
    % most CEST experiments, the number of pulses should be the same for all
    % offsets. Spoiling comes at max. with every single pulse --> not an
    % additional issue at least - smae holds true for delays ... . 

        % we had introduced NaN in case that the number of events is not the same
        % for all offsets. 
        if any(isnan(eventTable_ID))
            error(sprintf('in WrintingCEST presatToPPG.m:\n\t<<<number of events (Rf, gradient & delay) must be identical for all offsets!>>>'));
        end
        % check that all offsets have same order of RF, delay and gradient events:
        for nn=1:size(eventTable_ID,1)
            if numel(unique(eventTable_ID(nn,:))) ~= 1
                error(sprintf('in WritingCESTpresatToPGG.m:\n\t<<<similar order of events (Rf,delay & gradient) is requiered for all offsets!);\n'));
                fprintf('This is what your events look like (1st dim: events --- 2nd dim: offsets) :\n');
                eventTable_ID
            end
        end
        % check that same pulse shapes are used for all offsets 
        dwnfld = NaN;
        upfld = dwnfld;
        isDownField = [0,0];
        isUpField   = [0,0];
        isWater     = 0;
        ok_IDsUF      = [];
        ok_IDsDF      = [];
        water_IDs   = [];

        for nn=1:size(eventTable_full,1)
            for kk=1:size(eventTable_full,2)
                if eventTable_ID(nn,1) == 1
                    %check RF with negative shift w.r.t. to bulk water
                    if eventTable_full{nn,kk}.offset < 0 && isnan(sum(dwnfld))
                        dwnfld = eventTable_full{nn,kk}.ID;
                        isDownField = [nn,kk];
                        ok_IDsDF  = cat(1,ok_IDsDF,eventTable_full{nn,kk}.ID);
                    elseif eventTable_full{nn,kk}.offset < 0
                        if any(~(eventTable_full{nn,kk}.ID == dwnfld))
                            error(sprintf('in WritingCESTpresatToPPG.m:\n\t>>>(-)all RF events need same pulse shape, at least for all down-/upfield offsets'));
                        end
                        ok_IDsDF  = cat(1,ok_IDsDF,eventTable_full{nn,kk}.ID);
                    end % -ppm

                    %same for +ppm range
                    if eventTable_full{nn,kk}.offset > 0 && isnan(sum(upfld))
                        upfld = eventTable_full{nn,kk}.ID;
                        isUpField = [nn,kk];
                        ok_IDsUF  = cat(1,ok_IDsUF,eventTable_full{nn,kk}.ID);
                    elseif eventTable_full{nn,kk}.offset > 0
                        if any(~(eventTable_full{nn,kk}.ID ==upfld))
                            error(sprintf('in WritingCESTpresatToPPG.m:\n\t>>>(+)all RF events need same pulse shape, at least for all down-/upfield offsets'));
                        end
                        ok_IDsUF  = cat(1,ok_IDsUF,eventTable_full{nn,kk}.ID);
                    end % +ppm

                    if eventTable_full{nn,kk}.offset == 0
                        water_IDs = cat(1,water_IDs,eventTable_full{nn,kk}.ID);
                        isWater = 1;
                    end
                end %it was a RF event
            end% loop along offsets
            dwnfld=NaN;upfld=NaN;
        end%loop along "event"-direction

        %  might be that all pulses were on-resonant (e.g. SATREC) --> in this case
        %  we don't have positive and/or negative offsets. Only need to make
        %  sure that the on-resonant pulse always has the same shape.
        if isempty(ok_IDsDF) && isempty(ok_IDsUF)
            fprintf('\nin WritingCESTpresatToPPG.m:\n\t>>>INFO: found solely on-resonant pulses!\n');
            if size(unique(water_IDs,'rows','stable'),1)>1
                error('\in WriteCESTpresatToPPG.m:\n\tall on-resonant pulses must have the same shape when being played out for multiple "offsets"!\n');
            end
            water_IDs = unique(water_IDs,'rows','stable');
            ok_IDsUF  = water_IDs;
            ok_IDsDF  = [0 0];
            isUpField = [1,1];
        end

        %make sure water's IDs are contained in either up or downfield's IDs
        water_IDs   = unique(water_IDs,'rows','stable');
        ok_IDsDF    = unique(ok_IDsDF,'rows','stable');
        ok_IDsUF    = unique(ok_IDsUF,'rows','stable');



        if isWater && sum(sum(ok_IDsDF - water_IDs)) == 0  
            fprintf('WritingCESTpresatToPPG.m: \n<<<<INFO>>>: on-resonant pulses are assigned same pulse shapes as downfield pulses!\n');
            isWater = -1;
        elseif isWater && sum(sum(ok_IDsUF - water_IDs)) == 0 
            fprintf('WritingCESTpresatToPPG.m: \n<<<<INFO>>>: on-resonant pulses are assigned same pulse shapes as upfield pulses!\n');
            isWater = 1;
        elseif isWater
            error(sprintf('\nin: WritingCESTpresatToPPG.m:\n\tthe on-resonant saturation must have same pulse shape(s) as either up- or downfield saturation!\n'));
        end


        BothOffsets =[NaN,NaN];
        if sum(isUpField)
            BothOffsets(1,1) = isUpField(1,2);
        end
        if sum(isDownField)
            BothOffsets(1,2) = isDownField(1,2);
        end
    % switch for up-/downfield presaturation
        if isWater < 0
            fprintf(fid,'if "fCEST <= 0" goto downfield\n'); 
        else
            fprintf(fid,'if "fCEST < 0" goto downfield\n'); 
        end
        if isWater > 0
            fprintf(fid,'if "fCEST >= 0" goto upfield\n'); 
        else
            fprintf(fid,'if "fCEST > 0" goto upfield\n'); 
        end

        whichOff = 0;
        for nOffset = BothOffsets %Bruker crashes if files are too large --> can't have different offsets...
            whichOff = whichOff + 1;
            DoesNotExist = 1;
            if whichOff == 1 
                if ~isnan(nOffset)
                    fprintf(fid,'\nupfield,\n');
                    DoesNotExist = 0;
                end 
            elseif whichOff == 2
                if ~isnan(nOffset)
                    fprintf(fid,'\ndownfield,\n');
                    DoesNotExist = 0;
                end 
            end

            if ~DoesNotExist
                for nCESTprst=1:size(eventTable_ID,1)  % over events within offset        

                    if eventTable_ID(nCESTprst,nOffset)==0      % delay
                        fprintf(fid,'dCEST dCEST.inc\n');

                    elseif eventTable_ID(nCESTprst,nOffset) > 0 % pulse
                        fprintf(fid,'10u FArfCEST:f1\n(10u fCEST):f1\n10u fCEST.inc\n');
                        internalID = eventTable_full{nCESTprst,nOffset}.ID; 
                        internalID = ismember(unique_rf,internalID,'rows');
                        internalID = find(internalID);
                        if numel(internalID) ~= 1
                            error('could not assign baselevel pulse shape to this pulse ID!');
                        end
                        externalID = BaseParamIndex(internalID); % numbers assigned via SetBaseLevelParameters.c; e.g. sp0,sp1,...
                        fprintf(fid,'5u   gatepulse 1\n(rfCEST:sp%i(currentpower) pCEST):f1\n',externalID);
                        fprintf(fid,'0u FArfCEST.inc\n10u rfCEST.inc pCEST.inc\n');

                    elseif eventTable_ID(nCESTprst,nOffset) < 0 %gradient
                        warning('similar spoiler gradients along x,y and z supported only!\n')
                        tRampUp     = seq.gradLibrary.data(eventTable(nCESTprst,nOffset)).array(2)*10^6;
                        tFlat       = seq.gradLibrary.data(eventTable(nCESTprst,nOffset)).array(3)*10^6;
                        tRampDown   = seq.gradLibrary.data(eventTable(nCESTprst,nOffset)).array(4)*10^6;
                        amp         = seq.gradLibrary.data(eventTable(nCESTprst,nOffset)).array(1);
                        if ~exist('SysProp','var')
                            SysProp.gradient.maxAmp = amp;%[Hz/m]
                            warning(sprintf('\n\tit is highly recommended to provided your system limits!!!\n\tgradient will be set to maximum now!\n\tcheck if that is ok!\n'));
                        end
                        % Bruker: amplitudes in [%] of max possible value
                        amp = amp/SysProp.gradient.maxAmp*100 ;
                        ramp        = max(tRampUp);
                        %had trouble with amplitudes! quick 'n dirty fix:
                        if abs(amp)>=100
                           warning('"WritingCESTpresatToPPG.m" >>> Trouble with too large amplitudes...this should never happen');
                           amp = sign(amp)*25; %on our system 25% is fine
                        end
                        fprintf(fid,'%.0fu grad_ramp{%.3f, %.3f, %.3f}\n',ramp,amp,amp,amp); % [us] time for ramp up & strength of gradient
                        fprintf(fid,'%du\n',round(tFlat));% [us] hold gradient=on for flat time
                        if tRampDown > 0
                            fprintf(fid,'%du grad_off\n',round(tRampDown)); 
                        else
                            fprintf(fid,'d2 grad_off\n'); %
                            warning('user specified reampdown time smaller or equal to zero! Will use time "d2" instead!');
                        end
                    end
                end % single event within offset

                % at the end of the offset: we need to go to the RO
                fprintf(fid,'goto goToRO\n\n\n');

            else % either up or downfield was existing 
                if whichOff == 1 %UF
                    fprintf(fid,'\nupfield,\ngoto goToRO\n\n');
                elseif whichOff == 2 % DF
                    fprintf(fid,'\ndownfield,\ngoto goToRO\n\n');
                end
            end
        end % offset


        % here are the last "after CEST" commaneds( resat phase and frequency
        % for RO for instance...)
        fprintf(fid,'\ngoToRO,\n;reset freq for image acquisition\n');
        fprintf(fid,'10u fq8(receive):f1\n'); % reset frequency
        fprintf(fid,'10u pl1:f1\n');          % reset power
        % IMPORTANT: go to "stopCEST which is outside the CEST loop!! otherwise
        % infinite loop!
        fprintf(fid,'\n"CEST_cnt = CEST_cnt + 1"\ngoto stopCEST\n\n');
        fprintf(fid,'\n; all these commands must be reached by explicit "goto"!\n');

        % prior to versy first pulse: reset all array pointers
        fprintf(fid,'labF,\n0u fCEST.res\ngoto labF2\nlabP,\n0u pCEST.res\ngoto labP2\nlabRF,\n0u rfCEST.res\ngoto labRF2\nlabD,\n0u dCEST.res\ngoto labD2\nlabFA,\n0u FArfCEST.res\ngoto labFA2\n');
        fclose(fid);
        fprintf(['former pulseq-CEST-presaturation (*.seq) was translated to:\n\t',strrep(floc,'\','\\'),'\\CEST\\\n\t as: CEST_arbitrary.ppg\n']);
        statusID = 1;
    catch % on error
        statusID = 0;
    end
end
