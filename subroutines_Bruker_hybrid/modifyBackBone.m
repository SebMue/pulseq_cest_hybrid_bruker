function [ResID] = modifyBackBone(numCESTpulses,methodDirectory)
% [ResID] = modifyBackBone(numCESTpulses,methodDirectory)
% this function will provide a handle in backbone.c that is necessary to
% TODO: update pulses(?); check if we actually need this!!!
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------
  try
    [tline] = readFileSimple([methodDirectory,'\backbone.c']);
    indx=[];
    for nn=1:size(tline,2)
        temp_line = strtrim(tline{nn});
        if contains(tline{nn},'void backbone(') && strcmp(temp_line(1:14),'void backbone(') 
            indx=[indx,nn];
        end
    end
    indx = indx(end);
    if isempty(indx)
        error('could not find line to insert code!!!');
    else
       if strcmp(strtrim(tline{indx(end)+1}),'{')
           indx = indx +1;
       end
    end
    if strcmp(strtrim(tline{indx+1}),'}') || strcmp(strtrim(tline{indx+1}),'{')
        indx=indx+1;
        warning('may have trouble setting "STB_UpdateRFPulse" --- check backbone.c line %.0d carefully',indx);
    end
    fid = fopen([methodDirectory,'\CEST\backbone.c'],'w');
    for kk=1:indx(end)
       temp = strrep(tline{kk},'%','%%');
       temp = strrep(temp,'\n','\\n');
       fprintf(fid,[temp,'\n']);
    end
    
    % update RF opulses: (necessary?)
    fprintf(fid,'\n/* -----------------------CEST WIP---------------------------*/\n');
    fprintf(fid,'/*NO WARRANTY on this code! Corrections: sebastian.mueller<at>tuebingen.mpg.de*/\n');
    fprintf(fid,'\tDB_MSG(("including CEST relations into backbone.c"));\n');
    for nP=1:numCESTpulses
       fprintf(fid,'\tSTB_UpdateRFPulse("CESTpulse%02.f", 1, PVM_DeriveGains, Conventional);\n',nP-1); 
    end
    
    % call all relations to make sure values get read from *.cest file!
    fprintf(fid,'/*these are necessary to fill the initialized arrays with values! We don not use any other initialization!*/\n');
    fprintf(fid,'\tCEST_RFduration_relation();\n');
    fprintf(fid,'\tCEST_delay_relation();\n');
    fprintf(fid,'\tCEST_phase_relation();\n');
    fprintf(fid,'\tCEST_pwrEquiv_relation();\n');
    fprintf(fid,'\tCEST_pulseID_relation();\n');
    fprintf(fid,'\tCEST_freq_relation();\n');
    fprintf(fid,'\tCEST_TotTime_relation();\n');
    fprintf(fid,'\tCEST_pwrIntRat_relation();\n');
    fprintf(fid,'\tCEST_power_relation();/*call this after the other relations were called!*/\n');
    fprintf(fid,'/* -----------------------CEST WIP---------------------------*/\n');
    
    for kk=indx(end)+1:size(tline,2)
       temp = strrep(tline{kk},'%','%%');
       temp = strrep(temp,'\n','\\n');
       fprintf(fid,[temp,'\n']);
    end
    
    fclose(fid);
    ResID=1;
    
  catch %on error:
      ResID = 0;
      try
          fclose(fid);
      catch
          fprintf('could not close file .../CEST/backbone.c');
      end
  end
end % main function