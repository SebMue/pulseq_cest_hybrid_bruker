function [ResID] = modifyBackBone_timing(methodDirectory)
% [ResID] = modifyBackBone_timing(methodDirectory)
% !! call this function AFTER modifyBackBone.m was executed !!
% Function will add the duration of ll CEST events (RF, delay,
% gradients,... to the total scan time; this is e.g. needed for gradient DC
% simulations on Bruker systems.
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------

  try
    [tline] = readFileSimple([methodDirectory,'\CEST\backbone.c']);
    indx=[];
    % figure out where backbone(void) is called
    for nn=1:size(tline,2)
        temp_line = strtrim(tline{nn});
        if contains(tline{nn},'SetRecoParam(') && strcmp(temp_line(1:13),'SetRecoParam(') 
            indx=[indx,nn];
        end
    end
    if numel(indx) ~= 1
        error('could not modify code for scan time duration in backbone.c!');
    end
    fid = fopen([methodDirectory,'\CEST\backbone.c'],'w');
    for nn=1:indx
        temp = strrep(tline{nn},'%','%%');
        temp = strrep(temp,'\n','\\n');
        fprintf(fid,[temp,'\n']);
    end
    fprintf(fid,'PVM_ScanTime += CEST_TotTime;\n');
    for nn=indx+1:size(tline,2)
        temp = strrep(tline{nn},'%','%%');
        temp = strrep(temp,'\n','\\n');
        fprintf(fid,[temp,'\n']);
    end
    fclose(fid);
    ResID = 1;
    fprintf('including the duration of ALL presaturation pulses and delays to scan time calculations (in: backbone.c)');
  catch %on error:
      ResID = 0;
      try
        fclose(fid);
      catch
          fprintf('could not close file .../CEST/backbone.c\n');
      end
      warning(sprintf('\n\tcould not find identifier in backbone.c for modifying total scan time!\n\tprobably the gradient DC simulation will not work!\n\tthis means CEST presaturation is not included in sequence duration calculation!\n'));
  end
end %main funciton