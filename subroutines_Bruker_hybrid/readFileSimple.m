% simple read text file
% requieres full absolute name of file!
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m -----------
function [tline] = readFileSimple(fname)
    fid = fopen(fname,'r')    ;
    n = 1;
    while ~feof(fid)
        tline{n} = fgetl(fid);
        n = n+1;
    end
    fclose(fid);
end