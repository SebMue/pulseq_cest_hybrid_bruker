function [ResID] = introduceRFpulse(numCESTpulses,methodDirectory)
% [ResID] = introduceRFpulse(numCESTpulses,methodDirectory)
% input is number of different CEST pulse shapes (integer) and character
% string that contains the path to the methods directory of the RO;
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------

    try
        % figure out how many pulse objects are already assigned by
        % readout:
        [tline] = readFileSimple([methodDirectory,'\parsDefinition.h']);
        indx=[]; % figure out where we can insert our text (may not work)
        for n=1:size(tline,2)
            if contains(tline{n},'/*	E N D   O F   F I L E')
                indx=[indx,n];
            end
        end
        if isempty(indx)
            fprintf('HINT:\n\tcould not find the exact match for "end of file" (character string);\n\tshould not be a problem, will place our code at the end of the file!\n');
            indx = size(tline,2);
        end
        % modify file:
        fprintf('defining CEST pulses in parsDefinition.h');
        here=cd;
        cd(methodDirectory);
        if ~isdir('CEST')
            mkdir CEST
        end
        cd(here)
        fid = fopen([methodDirectory,'\CEST\parsDefinition.h'],'w');
        for kk=1:indx-2
           temp = strrep(tline{kk},'%','%%');
           temp = strrep(temp,'\n','\\n');
           fprintf(fid,[temp,'\n']);
        end
        fprintf('copied original file!\n')
        fprintf('adding CEST pulses!\n')
        % include all parameters we need:
        fprintf(fid,'/* -----------------------CEST WIP---------------------------*/\n');
        fprintf(fid,'/*these pulses are defined for the CEST-pulseq-hybrid! \n');
        fprintf(fid,'NO WARRANTY FOR THIS CODE! WIP USAGE ONLY!\n');
        fprintf(fid,'sebastian.mueller<at>tuebingen.mpg.de*/\n');
        % introduce custom pulses as Bruker pulse objects:
        for nP=1:numCESTpulses
            fprintf(fid,'PVM_RF_PULSE parameter\n');
            fprintf(fid,'{\n');
            fprintf(fid,'\tdisplay_name "CEST_pulse%02.f";\n',nP-1);
            fprintf(fid,'\trelations    CESTpulse%02.fRelation;\n',nP-1);
            fprintf(fid,'\teditable false;\n');
            fprintf(fid,'}CESTpulse%02.f;\n',nP-1);  
            fprintf(fid,'\n');
        end

        % introduce CEST parameters:
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST saturation power";\n\trelations CEST_power_relation;\n\tunits "W";\n\tformat "%%.6f";\n}CEST_pwrLIST[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST interpulse delay";\n\trelations CEST_delay_relation;\n\tunits "s";\n\tformat "%%.6f";\n}CEST_DelayList[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST RF duration";\n\trelations CEST_RFduration_relation;\n\tunits "us";\n\tformat "%%.6f";\n}CEST_RFduration[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST phase list";\n\trelations CEST_phase_relation;\n\tformat "%%.6f";\n}CEST_PhaseList[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST offset list";\n\trelations CEST_freq_relation;\n\tunits "Hz";\n\tformat "%%.6f";\n}CEST_freqList[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST power equivalents";\n\trelations CEST_pwrEquiv_relation;\n\tformat "%%.6f";\n}CEST_pwrEquiv[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST pulse IDs";\n\trelations CEST_pulseID_relation;\n\tformat "%%.0f";\n}CEST_pulseID[];\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "total CEST time";\n\trelations CEST_TotTime_relation;\n\tformat "%%.6f";\n\tunits "ms";\n}CEST_TotTime;\n');
        fprintf(fid,'double parameter\n{\n\tdisplay_name "CEST FA equivalents";\n\trelations CEST_pwrIntRat_relation;\n\tformat "%%.6f";\n\teditable false;\n}CEST_pwrIntRat[];\n');
        fprintf(fid,'/* -----------------------CEST WIP---------------------------*/\n');
        if indx ~= size(tline,2)
            for kk=indx-1:size(tline,2)
               temp = strrep(tline{kk},'%','%%');
               temp = strrep(temp,'\n','\\n');
               fprintf(fid,[temp,'\n']);
            end
        end
        fclose(fid);
        ResID = 1;
    catch %on error:
      ResID=0;
      try
          fclose(fid);
      catch
          fprintf('could not close file parsDefinitions.d');
      end
    end 
end %main function