function [ResID]= modifyparsRelations(numCESTpulses,methodDirectory,CEST_rf,CEST_rfFA,CESTphasearray,CEST_delays,CEST_offArray,CEST_pwrEquiv,CEST_pulseID,CEST_pwrIntRat)
% [ResID]= modifyparsRelations(numCESTpulses,methodDirectory,CEST_rf,CEST_rfFA,CESTphasearray,CEST_delays,CEST_offArray,CEST_pwrEquiv,CEST_pulseID,CEST_pwrIntRat)
% This function will define all relations necessary to correctly set up
% CEST parameters. This will also define how/where to read *.cest files
% from.
% Second purpose of this function: create *.cest files that contain
% information on CEST parameters and will be read on the Bruker scanner.
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------
    try
        [tline] = readFileSimple([methodDirectory,'\parsRelations.c']);

        % as file only contains function defines: attach ours at the end of the file
        indx=[];%to figure out where we can insert our text
        for n=1:size(tline,2)
            if contains(tline{n},'E N D   O F   F I L E')
                indx=[indx,n];
            end
        end
        if isempty(indx)
            fprintf('HINT: \n\tparsRelations.c --- file looks other than expected!\n\tno "end of file" character line was found.\n\tWill attach to end of file; should not be a problem!\n');
            indx = size(tline,2)+1; 
        end


        if contains(tline{indx-1},'************************')
            indx = indx-2;
        else
            indx = indx-1;
        end
        
% write CEST parameters into *.cest files
        fid_off = fopen([methodDirectory,'\CEST\offset.cest'],'w');
        for nn=1:numel(CEST_offArray)
            fprintf(fid_off,'%.6f,',CEST_offArray(nn));
        end;fclose(fid_off);
        fid_pha = fopen([methodDirectory,'\CEST\phase.cest'],'w');
        for nn=1:numel(CESTphasearray)
            fprintf(fid_pha,'%.6f,',CESTphasearray(nn)*360/2/pi); % needs to be in deg for Bruker ?
        end;fclose(fid_pha);
        fid_peq = fopen([methodDirectory,'\CEST\pwrequ.cest'],'w');
        for nn=1:numel(CEST_pwrEquiv)
            fprintf(fid_peq,'%.6f,',CEST_pwrEquiv(nn));
        end;fclose(fid_peq);
        fid_PID = fopen([methodDirectory,'\CEST\pulseid.cest'],'w');
        for nn=1:numel(CEST_pulseID)
            fprintf(fid_PID,'%.6f,',CEST_pulseID(nn));
        end;fclose(fid_PID);
        fid_del = fopen([methodDirectory,'\CEST\delay.cest'],'w');
        for nn=1:numel(CEST_delays)
            fprintf(fid_del,'%.6f,\n',CEST_delays(nn));
        end; fclose(fid_del);
        fid_pwr = fopen([methodDirectory,'\CEST\peakB1.cest'],'w');
        for nn=1:numel(CEST_rfFA)
            fprintf(fid_pwr,'%.6f,',CEST_rfFA(nn));
        end;fclose(fid_pwr);
        fid_dur = fopen([methodDirectory,'\CEST\puldur.cest'],'w');
        for nn=1:numel(CEST_rf)
            fprintf(fid_dur,'%.6f,\n',CEST_rf(nn)*10^6);
        end;fclose(fid_dur);
        fid_dur = fopen([methodDirectory,'\CEST\IntEqu.cest'],'w');
        for nn=1:numel(CEST_pwrIntRat)
            fprintf(fid_dur,'%.6f,\n',CEST_pwrIntRat(nn));
        end;fclose(fid_dur);
        fclose all; %need to close all other files first!
        
% modify Bruker's parsRelations.c file to add relations for CEST presaturation:        
        fid = fopen([methodDirectory,'\CEST\parsRelations.c'],'w');
        if fid<0
            error('could not access/open file ...\CEST\parsRelations.c');
        end

        for kk=1:indx
           temp = strrep(tline{kk},'%','%%');
           temp = strrep(temp,'\n','\\n');
           fprintf(fid,[temp,'\n']);
        end

        fprintf(fid,'\n/* -----------------------CEST WIP---------------------------*/\n');
        fprintf(fid,'/*NO WARRANTY on this code! Corrections: sebastian.mueller<at>tuebingen.mpg.de*/\n');
        % first we'll define the function that can read our *.cest parameter files 
        fprintf(fid,'static inline void CEST_readFile_simple(double *param,char *fName,int SZ)\n{\n\tchar *pPath;\n\tpPath = getenv ("USER");\n\tchar sString[256] = "/opt/PV6.0.1/prog/curdir/";\n');
        fprintf(fid,'\tstrcat(sString, pPath);\n\tstrcat(sString, "/ParaVision/methods/src/CEST_MPI/");\n\tstrcat(sString,fName);\n\tstrcat(sString,".cest");\n\tDB_MSG(("reading file:"));\n');
        fprintf(fid,'\tDB_MSG((sString));\n\tFILE *myFile;\n\tmyFile = fopen(sString, "r");\n\tif (myFile == NULL)\n\t{\n\t\tDB_MSG(("ERROR READING FILE:"));\n\t\tDB_MSG((sString));\n\t\tfclose(myFile);\n\t\texit (0);\n\t}\n');
        fprintf(fid,'\tfor (int nn = 0; nn < SZ; ++nn)\n\t{\n\t\tfscanf(myFile, "%%lf,", &param[nn]);\n\t}\n\tfclose(myFile);\n}\n');
        % basic pulse object relations
        for nP=1:numCESTpulses
            fprintf(fid,'void CESTpulse%02.fRelation(void)\n',nP-1);
            fprintf(fid,'{\n');
            fprintf(fid,'\tDB_MSG(("-->CESTpulse%02.fRelation"));\n\n',nP-1);
            fprintf(fid,'\t/*\n\t* Tell the request handling system that the parameter\n\t* CESTpulse%02.fRelation has been edited\n\t*/\n\n',nP-1);
            fprintf(fid,'\tUT_SetRequest("CESTpulse%02.f");\n\n',nP-1);
            fprintf(fid,'\t/*\n\t* call the backbone; further handling will take place there\n\t* (by means of STB_UpdateRFPulse)\n\t*/\n\n');
            fprintf(fid,'\tbackbone();\n\n');
            fprintf(fid,'\tDB_MSG(("<--CESTpulse%02.fRelation "));\n\n',nP-1);
            fprintf(fid,'}\n');
        end

        % make sure user is familiar with Bruker systems!
        fprintf('\n-*-*-*-*-*-*-\n\tWARNING\n\tuser MUST  carefully check the power limits set in parsRelations file!!!\n\tThese may cause severe damage of the scaner hardware if set incorrect!!!!\n -*-*-*-*-*-*-\n');
        answer = questdlg('user MUST check power settings (single parameter "CEST_pwrLIST") before sequence runs at scanner!!!','GUI','accept','requierement unclear','requierement unclear');
        if strcmp(answer,'requierement unclear')
            error('not safe to run this WIP sequence if user is not aware of how to control RF power!!');
        end


        %%%%%%%% delays %%%%%%%%%%%
        fprintf(fid,'void CEST_delay_relation(void)\n{\n\tPARX_change_dims("CEST_DelayList",    %.0f);\n\tchar fName[] = "delay";\n\tCEST_readFile_simple(CEST_DelayList,fName,%.0f);\n}\n',numel(CEST_delays),numel(CEST_delays));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% pulse durations %%%
        fprintf(fid,'void CEST_RFduration_relation(void)\n{\n\tPARX_change_dims("CEST_RFduration", %.0f);\n\tchar fName[] = "puldur";\n\tCEST_readFile_simple(CEST_RFduration,fName,%.0f);\n}\n',numel(CEST_rf),numel(CEST_rf));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% rf powers %%%%%%%%%
        fprintf(fid,'void CEST_power_relation(void)\n{\n\tPARX_change_dims("CEST_pwrLIST", %.0f);\n\tchar fName[] = "peakB1";\n\tCEST_readFile_simple(CEST_pwrLIST,fName,%.0f);\n',numel(CEST_rfFA),numel(CEST_rfFA));
        fprintf(fid,'\tdouble CEST_gamma = 42.5774785; //NIST in [MHz/T]\n');
        fprintf(fid,'\tYesNo CEST_RefPowAvailable = No; /*might be impossible in simulation*/\n\tdouble CEST_RefPower = 0.0;\n\tCEST_RefPowAvailable = STB_GetRefPower(1, &CEST_RefPower);\n');
        fprintf(fid,'\tif (CEST_RefPowAvailable == No)\n\t{\n\t\tCEST_RefPower = 0; /*!! MUST be set to 0 for experiments at scanner !!*/\n\t}\n');
        fprintf(fid,'\n\tfor (int nn = 0;nn < %.0f; ++nn)\n\t{\n\t\tCEST_pwrLIST[nn] = CEST_RefPower * pow(CEST_pwrLIST[nn],2) / pow((double)90/(double)0.001/(double)360/CEST_gamma,2);\n\t\tCEST_pwrLIST[nn] = MIN_OF(MAX_OF(CEST_pwrLIST[nn], 0), 10);\n\t}\n}\n',numel(CEST_rfFA));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% phases %%%%%%%%%%%%
        fprintf(fid,'void CEST_phase_relation(void)\n{\n\tPARX_change_dims("CEST_PhaseList", %.0f);\n\tchar fName[] = "phase";\n\tCEST_readFile_simple(CEST_PhaseList,fName,%.0f);\n}\n',numel(CESTphasearray),numel(CESTphasearray));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% offsets %%%%%%%%%%%
        fprintf(fid,'void CEST_freq_relation(void)\n{\n\tPARX_change_dims("CEST_freqList", %.0f);\n\tchar fName[] = "offset";\n\tCEST_readFile_simple(CEST_freqList,fName,%.0f);\n}\n',numel(CEST_offArray),numel(CEST_offArray));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%% pulse-IDs %%%%%%%%%
        fprintf(fid,'void CEST_pulseID_relation(void)\n{\n\tPARX_change_dims("CEST_pulseID", %.0f);\n\tchar fName[] = "pulseid";\n\tCEST_readFile_simple(CEST_pulseID,fName,%.0f);\n}\n',numel(CEST_pulseID),numel(CEST_pulseID));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%
        
        %%%%%%% FA equivalents %%%%
        fprintf(fid,'void CEST_pwrIntRat_relation(void)\n{\n\tPARX_change_dims("CEST_pwrIntRat", %.0f);\n\tchar fName[] = "IntEqu";\n\tCEST_readFile_simple(CEST_pwrIntRat,fName,%.0f);\n}\n',numel(CEST_pwrIntRat),numel(CEST_pwrIntRat));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%    

        %%%%%%% power equivalents %
        fprintf(fid,'void CEST_pwrEquiv_relation(void)\n{\n\tPARX_change_dims("CEST_pwrEquiv", %.0f);\n\tchar fName[] = "pwrequ";\n\tCEST_readFile_simple(CEST_pwrEquiv,fName,%.0f);\n}\n',numel(CEST_pwrEquiv),numel(CEST_pwrEquiv));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%    
        
        %%%%%%% total time %%%%%%%%
        fprintf(fid,'void CEST_TotTime_relation(void)\n{\n\tCEST_TotTime = 0;\n\tfor (int nn=0; nn<%.0f; ++nn)\n\t{\n\t\tCEST_TotTime += CEST_DelayList[nn]*1000; //this is [s]\n\t}\n\t for (int nn=0; nn < %0.f; ++nn)\n\t{\n\t\tCEST_TotTime += CEST_RFduration[nn]/1000;// this is [us]\n\t}\n\tDB_MSG(("cummulative scan time for CEST was calculated:"));\n\tprintf("%%.2f [ms]\\n",CEST_TotTime);\n}\n',numel(CEST_delays),numel(CEST_rf));
        %%%%%%%%%%%%%%%%%%%%%%%%%%%

        % remaining parts of original file:
        fprintf(fid,'/* -----------------------CEST WIP---------------------------*/\n\n');
        if indx ~= size(tline,2)
            for kk=indx+1:size(tline,2)
               temp = strrep(tline{kk},'%','%%');
               temp = strrep(temp,'\n','\\n');
               fprintf(fid,[temp,'\n']);
            end
        end

        fclose(fid);
        ResID = 1;
    catch %on error:
        ResID = 0;
        try
            fclose(fid);
            fclose(fid_off);
            fclose(fid_pha);
            fclose(fid_peq);
            fclose(fid_PID);
            fclose(fid_del);
            fclose(fid_pwr);
            fclose(fid_dur);
        catch
            fprintf('could not close file(s)\n');
        end
    end
end % main function