function [freeParams] = check4freeRF(tline,VersInfo)
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m -----------
    cnt = 1;
    for nn=1:size(tline,2)
        if contains(tline{nn},'"ACQ_RfShapes[') %important to have " and [ symbols!
            blockedParams{1,cnt}=tline{nn};
            cnt = cnt +1 ;
        end
    end
    for kk=1:size(blockedParams,2)
       strt = strfind(blockedParams{1,kk},'ACQ_RfShapes');
       stp  = strfind(blockedParams{1,kk},'[');
       strt = min(stp(stp>strt));
       stp  = strfind(blockedParams{1,kk},']');
       stp  = min(stp(stp>strt));
       blcked(kk) = str2num(blockedParams{1,kk}(strt+1:stp-1)); 
    end
    % remove read out pulses 
    if strcmp(VersInfo,'PV6')
        freeParams=0:63;
        freeParams(blcked+1)=[];
        sprintf('Found that at least %02.f/%2.f RF shapes are already assigned to baselevel params! Will not use these!',numel(blcked),64)
    else
        freeParams=0:15;
        freeParams(blcked+1)=[];
        warning(sprintf('\n\tno version detected / PV5 was selected!\n\tWill use n=16 possible RF shapes.'));
    end
end