function [ID] = modifyParsLayout(methodDirectory)
% [ID] = modifyParsLayout(methodDirectory)
% list all parameters in the parsLayout.h file which after the scan was
% performed will provid information on the acquisition parameters. New
% parameter group is defined for the CEST parameters!
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m ------------

    try
        [tline] = readFileSimple([methodDirectory,'\parsLayout.h']);

        indx=[];%to figure out where we can insert our text
        for n=1:size(tline,2)
            if contains(strip(tline{n}),'} MethodClass;')
                indx=[indx,n];
            end
        end
        indx = max(indx); %just to be sure... should actually be single position
        if isempty(indx)
            error(sprintf('while reading parsLayout.h:\n could not find "parcalss" named "MethodClass"\n'));
        end

        % check where we can create our own "class"
        PlaceHere = [];
        for nn=1:size(tline,2)-1
           if strcmp(strip(tline{nn}),'parclass') && strcmp(strip(tline{nn+1}),'{')
               PlaceHere = [PlaceHere,nn];
           end
        end
        PlaceHere = min(PlaceHere);
        % write modified file:
        fid = fopen([methodDirectory,'\CEST\parsLayout.h'],'w');

        TheCESTparams = 'parclass\n{\n\tCEST_DelayList;\n\tCEST_RFduration;\n\tCEST_PhaseList;\n\tCEST_TotTime;\n\tCEST_pwrLIST;\n\tCEST_freqList;\n\tCEST_pulseID;\n\tCEST_pwrEquiv;\n\tCEST_pwrIntRat;\n}CESTmethod;\n';

        if numel(PlaceHere)==1 && PlaceHere == 1
            fprintf(fid,TheCESTparams);
            for kk=1:indx
                temp = strrep(tline{kk},'%','%%');
                temp = strrep(temp,'\n','\\n');
                fprintf(fid,[temp,'\n']);
            end
            fprintf(fid,'\tCESTmethod;\n');
            for kk = indx:size(tline,2)
                temp = strrep(tline{kk},'%','%%');
                temp = strrep(temp,'\n','\\n');
                fprintf(fid,[temp,'\n']);
            end        
        else %this should typically happen
            for kk= 1:PlaceHere-1
                temp = strrep(tline{kk},'%','%%');
                temp = strrep(temp,'\n','\\n');
                fprintf(fid,[temp,'\n']);
            end
            % CEST param class:
            fprintf(fid,TheCESTparams);
            %remaining original file:
            for kk=PlaceHere:indx-1
                temp = strrep(tline{kk},'%','%%');
                temp = strrep(temp,'\n','\\n');
                fprintf(fid,[temp,'\n']);
            end
            fprintf(fid,'\tCESTmethod;\n');
            for kk = indx:size(tline,2)
                temp = strrep(tline{kk},'%','%%');
                temp = strrep(temp,'\n','\\n');
                fprintf(fid,[temp,'\n']);
            end          
        end
        fclose(fid);
        ID = 1;
        fprintf('all CEST parameters are written down in parsLAyout.h --> they should be accesible after scan!\n');
    catch %on error
        ID = 0;
        fclose all;
        error(sprintf('\tmodifyParsLayout.m:\n\t>>>something went wrong here!\n'));
    end 

end %main function