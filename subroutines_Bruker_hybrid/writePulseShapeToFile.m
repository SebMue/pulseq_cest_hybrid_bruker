
function [statusID,CEST_pwrEquiv,EvntTableToExcFile,CEST_pwrIntRat] = writePulseShapeToFile(seq,unique_rf,floc)
% -------- this is a subroutine of: CEST_Bruker_PulseqHybrid.m -----------
%% Write RF pulse files
    fprintf('Writing pulse files...\n');
    
    EvntTableToExcFile = ones(size(unique_rf,1),1);
    statusID = 0;

    fprintf('writing pulse shape files:  CEST_arbitrary.ppg\n');

    keys                = seq.rfLibrary.keys;
    
    % we'll pick "keys" such that we get the correct pulse depending on the
    % position within the "unique_rf" rather than the other way around! All
    % other sassignments are done w.r.t. "unique_rf"
    for nn=1:size(unique_rf,1)
        for kk=1:numel(keys)
            temp =seq.rfLibrary.data(keys(kk)).array(2:3);
            if sum(temp-unique_rf(nn,:))==0
                indx(nn) = keys(kk); 
            end
        end
    end
    
    keys = keys(indx);
    
    % currebtly unused
    CEST_pwrEquiv = zeros(1,size(unique_rf,1));
    
    % as we identified unique pulse shapes, we only need to create a single
    % shape file for each pulse; this is good because Bruker does not allow
    % arbitrary number of pulse shapes (e.g 64 in PV6)!
    for iPulse=1:size(unique_rf,1)       
        
        data        = seq.rfLibrary.data(keys(iPulse)).array;
%         amp         = data(1);
        magShape    = data(2);
        phaseShape  = data(3);

        shapeData               = seq.shapeLibrary.data(magShape).array;
        compressed.num_samples  = shapeData(1);
        compressed.data         = shapeData(2:end);
        mag                     = mr.decompressShape(compressed)*100;   % pecentage of maximum


        shapeData               = seq.shapeLibrary.data(phaseShape).array;
        compressed.num_samples  = shapeData(1);
        compressed.data         = shapeData(2:end);
        phase                   = mr.decompressShape(compressed)*360; % 0-360 degrees

        % on PV6 most pulse shape files (*.exc) contain 256 samples only;
        % we'll do the same here! If files are too large, scan will crash!
        if numel(mag)>256
            dwnsmplrt   = 256;
            origSize    = numel(mag);
            L1    = linspace(0,1,numel(mag));
            L2    = linspace(0,1,256);
            mag   = interp1(L1,mag,L2,'linear');
            phase = interp1(L1,phase,L2,'linear');
            fprintf('\nsince pulse was sampled at %08.f points, it was downsampled to %03.f points now!\n\n',origSize,dwnsmplrt);
            fprintf('for smooth shapes, e.g. Gaussian, this should not be an issue!\n');
        end


        integralRatio           = sum(mag)/100/length(mag);
        % currently unused:
        CEST_pwrEquiv(iPulse)   = sum((mag/100).^2)/length(mag); %this should be used for re-setting the Tx-channel B1, that is B1_chanel = B1_userInput * CEST_powerRatio >= B1_userInput
        CEST_pwrIntRat(iPulse)  = integralRatio;
        
        filename                = sprintf('CEST_pulse%02.f.exc',iPulse-1);
        fid                     = fopen([floc,'\CEST\',filename],'w');
        % write header of pulse shape file: 
        fprintf(fid,'##TITLE=%s\n',filename);
        fprintf(fid,'##JCAMP-DX= 5 BRUKER JCAMP library\n');
        fprintf(fid,'##DATA TYPE= Shape Data\n');
        fprintf(fid,'##ORIGIN= MATLAB\n');
        fprintf(fid,'##OWNER= <MPIpulseqCEST>\n');
        ID_exc = datestr(now);
        fprintf(fid,['##DATE= ',ID_exc(1:end-9),'\n']);
        fprintf(fid,['##TIME= ',ID_exc(end-7:end),'\n']);
        fprintf(fid,'##MINX= %.6e\n',min(mag));
        fprintf(fid,'##MAXX= %.6e\n',max(mag));
        fprintf(fid,'##MINY= %.6e\n',min(phase));
        fprintf(fid,'##MAXY= %.6e\n',max(phase));
        fprintf(fid,'##$SHAPE_EXMODE= Excitation\n');
        fprintf(fid,'##$SHAPE_TOTROT= 9.000000e+01\n');
        fprintf(fid,'##$SHAPE_BWFAC= 2.025000e+01\n');
        fprintf(fid,'##$SHAPE_INTEGFAC= %.6e\n',integralRatio);
        fprintf(fid,'##$SHAPE_REPHFAC= 50\n');
        fprintf(fid,'##$SHAPE_TYPE= conventional\n');
        fprintf(fid,'##$SHAPE_MODE= 0\n');
        fprintf(fid,'##NPOINTS= %d\n',length(mag));
        fprintf(fid,'##XYPOINTS= (XY..XY)\n');
        % mag and phase (this defines the shape):
        if size(mag,1) < size(mag,2)
            mag     = mag';
            phase   = phase';
        end
        dataVec = cat(2,mag,phase)';
        fprintf(fid,'%.6e, %.6e\n',dataVec);
        fprintf(fid,'##END=\n\n');
        
        % display pulse shape to user --- only for information purpose
        if ~ishandle(14)
            figure(14);
        end
        subplot(2,size(unique_rf,1),iPulse);                    plot(dataVec(1,:));title({'magnitude of pulse: ',strrep(filename,'_','\_')});
        subplot(2,size(unique_rf,1),iPulse+size(unique_rf,1));  plot(dataVec(2,:));title({'phase of pulse: ',strrep(filename,'_','\_')});

        fclose(fid);
        statusID = 1;
    end

end %main function